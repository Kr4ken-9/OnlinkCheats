﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Threading.Tasks;

/**
    NOTICE:
    The contents below this line were created specifically for this project. If you wanna use them in other projects, please contact GabrielTK Through GitLab issues (https://gitlab.com/GabrielTK/OnlinkCheats/issues)
*/

namespace OnlinkCheats
{
    class OnlinkDatabase
    {
        #region Fields
        
        public SQLiteConnection Connection;
        
        public Dictionary<string, int> AgentRanking = new Dictionary<string, int>
        {
            {"TERMINAL", 16},
            {"TechnoMage", 15},
            {"Veteran", 14},
            {"Expert", 13},
            {"Mage", 12},
            {"Elite", 11},
            {"Professional", 10},
            {"UberSkilled", 9 },
            {"Knowledgeable", 8 },
            {"Experienced", 7 },
            {"Skilled",6 },
            {"Intermediate",5 },
            {"Confident", 4},
            {"Novice", 3 },
            {"Beginner", 2 },
            {"Registered", 1 }
        };
        
        public int GetUplinkRatingInt(string rating) => AgentRanking[rating];
        
        #endregion
        
        #region Fundamentals

        public OnlinkDatabase(string path) => Connection = new SQLiteConnection($"Data Source = {@path}");

        public void Open() { Connection.Open();}
        
        public void Close() => Connection.Close();

        #endregion
        
        public async Task SetMoney(int Money, string Agent)
        {
            String query = "UPDATE bankaccount SET balance = @money where name = @name;";
            SQLiteCommand command = new SQLiteCommand(query, Connection);
            
            command.Parameters.AddWithValue("money", Money);
            command.Parameters.AddWithValue("name", Agent);
            
            await command.ExecuteNonQueryAsync();
        }

        public async Task SetAllRating(int uplinkRating = 16, int creditRating = 8, int uplinkScore = 2500)
        {
            String query =
                "UPDATE rating SET creditrating = @cr, uplinkrating = @rating, uplinkscore = @score where id = 9;";
            
            SQLiteCommand cmd = new SQLiteCommand(query, Connection);
            
            cmd.Parameters.AddWithValue("cr", creditRating);
            cmd.Parameters.AddWithValue("rating", uplinkRating);
            cmd.Parameters.AddWithValue("score", uplinkScore);
            
            await cmd.ExecuteNonQueryAsync();
        }
        
        public async Task SetUplinkRating(int uplinkRating = 16)
        {
            String query = "UPDATE rating SET uplinkrating = @rating where id = 9;";
            SQLiteCommand cmd = new SQLiteCommand(query, Connection);
            
            cmd.Parameters.AddWithValue("rating", uplinkRating);
            
            await cmd.ExecuteNonQueryAsync();
        }
        
        public async Task SetCreditRating(int creditRating = 8)
        {
            String query = "UPDATE rating SET creditrating = @cr where id = 9;";
            SQLiteCommand cmd = new SQLiteCommand(query, Connection);

            cmd.Parameters.AddWithValue("cr", creditRating);
            
            await cmd.ExecuteNonQueryAsync();
        }
        
        public async Task SetUplinkScore(int uplinkScore = 2500)
        {
            String query = "UPDATE rating SET uplinkscore = @score where id = 9;";
            SQLiteCommand cmd = new SQLiteCommand(query, Connection);
            
            cmd.Parameters.AddWithValue("score", uplinkScore);
            
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task SetAllSecuritySystemsStatus(bool Enabled)
        {
            int status = Enabled ? 1 : 0;
            String query = "UPDATE secsystem SET enabled = @status;";
            SQLiteCommand cmd = new SQLiteCommand(query, Connection);

            cmd.Parameters.AddWithValue("status", status);
            
            await cmd.ExecuteNonQueryAsync();
        }
    }
}
