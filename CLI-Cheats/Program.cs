﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using OnlinkCheats;

namespace CLI_Cheats
{
    internal class Program
    {
        static String Config(String AgentName) => $"~/.onlink/users/{AgentName}.db";
        
        public static void Main(string[] args)
        {
            String AgentName;
            
            Console.WriteLine("Please enter the name of an agent to modify.");

            AgentName = Console.ReadLine();

            if (!File.Exists(Config(AgentName)))
            {
                Console.WriteLine($"{AgentName} does not exist.");
                Main(args);
            }

            String Money;
            
            Console.WriteLine("Enter the amount of money to set your balance to.");

            Money = Console.ReadLine();

            if (!int.TryParse(Money, out int RealMoney))
            {
                Console.WriteLine($"{Money} is not an integer.");
                Main(args);
            }
            
            OnlinkDatabase DB = new OnlinkDatabase(Config(AgentName));

            SetMoney(DB, AgentName, RealMoney).GetAwaiter().GetResult();
        }

        static async Task SetMoney(OnlinkDatabase Database, String AgentName, int Money)
        {
            Database.Open();
            
            await Database.SetMoney(Money, AgentName);
            
            Database.Close();
        }
    }
}